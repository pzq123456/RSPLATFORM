# 云开发环境（后端）
> 使用腾讯云的轻量应用服务器开发，提供后端服务。
> 使用 gitlab 进行代码管理，使用 gitlab-ci 进行持续集成，使用 docker 进行容器化部署。

## python 的安装和配置
1. 调用脚本安装 python3.8.5
```bash
# 启动 init.sh 脚本，初始化环境
bash init.sh
```
2. 在当前文件夹下生成`venv`环境
```bash
# 生成 venv 环境
python3.8 -m venv venv

# pip 全局切换清华源
pip3.8 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
```

3.安装git centos7
```bash
yum install git
```

## 解决 gcc 库版本过低的问题
    
    wget https://repo.anaconda.com/archive/Anaconda3-2019.07-Linux-x86_64.sh
    sh Anaconda3-2019.07-Linux-x86_64.sh
    cp anaconda3/lib/libstdc++.so.6.0.26 /usr/lib64
    rm /usr/lib64/libstdc++.so.6
    ln -s /usr/lib64/libstdc++.so.6.0.26 /usr/lib64/libstdc++.so.6

    sudo yum install centos-release-scl
    sudo yum install devtoolset-7-gcc*
    scl enable devtoolset-7 bash
    which gcc
    gcc --version