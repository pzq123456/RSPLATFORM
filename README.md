# RSPLATFORM
> 这是2022年软件杯 A4（遥感解译）赛道参赛作品，我们是来自山东科技大学的 RSIDAS+97012744 队。

## part_1 非异步部分的运行步骤：
1. 请先安装requirements.txt :
        pip install -r requirements(python版本3.8.5)

> 请于百度网盘（链接："https://pan.baidu.com/s/1O5LEXmQrb3tHwgd23xvXzA "提取码：h9qv）自提ai解译算法的模型文件，并放置于aipro文件夹内。（注意缺失该文件会导致app无法成功启动！）

2. 上传的项目包含有数据库文件（app.db）但是数据库中写入的图片路径均已失效（图片体积过大没有上传），您可以先删除该数据库文件（即删除app.db）
      然后执行"(venv) $ flask db upgrade"命令即可生成新的空白数据库。（请确保您的计算机已经安装SQLite）
      或者启动项目后在python交互式终端中键入：
      "from app import db" 回车生效
      "db.create_all()" 回车生效
3、项目启动，如果您使用的是VScode（推荐使用），请选择打开文件夹，请直接打开“遥感解译平台”主文件夹（不要打开app文件夹，否则flask命令将无法生效）
```bash
(venv) $ flask run
```

- 直接点击运行，选择flask，选择默认app名称即可开始运行非异步模块，此时您已经可以进入系统页面，可以注册登录上传图片，注意此时点击功能按钮将会导致进度条卡死。

> *如果您使用的是非VScode，请在“遥感解译平台”主文件夹处打开终端（windows--powershell、cmd、Unix--bash）首先设置flask参数，然后执行falsk的run命令。


 ## part_2 异步部分运行步骤：
 注意：本项目实现了异步并发操作（falsk原生不支持异步操作）。本项目借助celery库及Redis服务器实现异步项目，因而需要配置异步服务。
  1、首先请安装Redis服务器，详细教程见"https://Redis.org "。（推荐Unix系统使用，Redis原生不支持Windows，但是可以使用WSL2子系统运行Unix安装上Redis）
  2、在app外部预先启动Redis服务器。
  3、在终端中输入命令"celery -A app.celery worker --loglevel=info -P eventlet -c 10"启动celery服务。（同样是“遥感解译平台”大文件夹）
 现在您已经成功启动项目了。
 如果启动失败，请在github上与我联系。如果觉得还不错欢迎给该项目点赞。
  

## appendix
### Redis 常用命令

```bash
# 启动
systemctl start redis

# 查看状态
systemctl status redis

# 停止
systemctl stop redis

# 重启
systemctl restart redis


# 进入客户端
redis-cli

# 成功连接并且进入客户端


# 添加数据
set test-key test-value
OK

# 查询数据
get test-key
"test-value"

# 删除数据
del test-key
(integer) 1

# 退出客户端
exit

redis-server -v
redis-server --version
```


